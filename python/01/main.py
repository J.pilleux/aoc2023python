coresp = {
    # Combos
    "oneight": "18",
    "twone": "21",
    "threeight": "38",
    "fiveight": "58",
    "eightwo": "82",
    "eighthree": "83",
    "sevenine": "79",
    "nineight": "98",
    # Simple
    "one": "1",
    "two": "2",
    "three": "3",
    "four": "4",
    "five": "5",
    "six": "6",
    "seven": "7",
    "eight": "8",
    "nine": "9",
}


def convert(line: str) -> str:
    for key, value in coresp.items():
        if key not in line:
            continue
        line = line.replace(key, value)

    return line


def main():
    values: list[str] = []
    with open("input.txt", "r") as cal_values:
        values = cal_values.readlines()

    total = 0
    for line in values:
        line = convert(line)
        numbers = [c for c in line if c.isdigit()]
        n1 = numbers[0]
        n2 = numbers[-1]
        total += int(f"{n1}{n2}")

    print(total)


if __name__ == "__main__":
    main()
