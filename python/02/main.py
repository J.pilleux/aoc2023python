maximums = {"red": 12, "green": 13, "blue": 14}


def to_num(color: str) -> int:
    color = color.strip()
    num, _ = color.split(" ", 2)
    return int(num)


class Game:
    num: int
    pulls: str
    power: int

    def __init__(self, num: str, pulls: str) -> None:
        self.num = int(num)
        self.pulls = pulls
        self.power = 0

        pulls = self.pulls.replace(";", ",")
        colors = pulls.split(",")
        reds = [to_num(s) for s in colors if "red" in s]
        greens = [to_num(s) for s in colors if "green" in s]
        blues = [to_num(s) for s in colors if "blue" in s]
        self.power = max(reds) * max(greens) * max(blues)

    def __str__(self) -> str:
        return f"Num: {self.num}, Colors: {self.pulls}"

    def is_possible(self) -> bool:
        pulls = self.pulls.split(";")
        for pull in pulls:
            colors = pull.split(",")
            for color in colors:
                color = color.strip()
                n, c = color.split(" ", 2)
                if int(n) > maximums[c]:
                    return False
        return True


def main():
    lines = []
    with open("input.txt", "r") as input:
        lines = input.readlines()

    res = 0
    for line in lines:
        meta, pulls = line.split(":", 2)
        _, num = meta.split(" ", 2)
        p = Game(num, pulls).power
        res += Game(num, pulls).power

    print(res)


if __name__ == "__main__":
    main()
