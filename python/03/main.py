def main():
    grid = []
    with open("input.txt", "r") as input:
        grid = input.readlines()

    number_positions: set[tuple[int, int]] = set()

    for x, line in enumerate(grid):
        for y, c in enumerate(line):
            if c == "\n" or c.isdigit() or c == ".":
                continue

            for xx in [x - 1, x, x - 1]:
                for yy in [y - 1, y, y + 1]:
                    if xx < 0 or xx >= len(grid) or yy < 0 or yy >= len(grid) or not grid[xx][yy].isdigit():
                        continue
                    while yy > 0 and grid[xx][yy - 1].isdigit():
                        yy -= 1
                    number_positions.add((xx, yy))

    print(number_positions)

    res = []
    for x, y in number_positions:
        sequence = ""
        while y < len(grid[x]) and grid[x][y].isdigit():
            sequence += grid[x][y]
            y += 1
        res.append(int(sequence))

    print(sum(res))


if __name__ == "__main__":
    main()
