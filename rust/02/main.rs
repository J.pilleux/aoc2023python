use phf::phf_map;

#[allow(unused_variables)]
static MAXIMUMS: phf::Map<&'static str, i32> = phf_map! {
    "red" => 12,
    "green" => 13,
    "blue" => 14
};

fn to_num(color_s: String) -> i32 {
    let color = color_s.trim();
    let num: Vec<&str> = color.splitn(2, " ").collect();
    return num[0].parse().unwrap();
}

#[allow(dead_code)]
struct Game {
    num: i32,
    pulls: String,
    power: i32
}

#[allow(dead_code)]
impl Game {
    pub fn new(num: String, pulls: String) -> Self {
        let num: i32 = num.parse().unwrap();
        let pulls = pulls.replace(";", ",");
        let colors = pulls.split(",");

        let reds: Vec<i32> = colors.clone().filter(|s| s.contains("red")).map(|s| to_num(String::from(s))).collect();
        let greens: Vec<i32> = colors.clone().filter(|s| s.contains("green")).map(|s| to_num(String::from(s))).collect();
        let blues: Vec<i32> = colors.clone().filter(|s| s.contains("blue")).map(|s| to_num(String::from(s))).collect();

        let power = reds.iter().max().unwrap() * greens.iter().max().unwrap() * blues.iter().max().unwrap();

        Self { num, pulls, power}
    }

    pub fn is_possible(&self) -> bool {
        let pulls = self.pulls.split(";");
        for pull in pulls {
            let colors = pull.split(",");
            for color in colors {
                let color = color.trim();
                let parts: Vec<&str> = color.split(" ").collect();
                let n: i32 = parts[0].parse().unwrap();
                let c = parts[1];
                if n > MAXIMUMS[c] {
                    return false
                }
            }
        }
        return true;
    }
}

fn main() {
    let content = include_str!("./input.txt");
    let values = content.split("\n");

    let mut res = 0;

    for line in values {
        if line.is_empty() {
            continue;
        }
        let parts: Vec<&str> = line.splitn(2, ":").collect();
        let meta = parts[0];
        let pulls = parts[1];

        let parts: Vec<&str> = meta.splitn(2, " ").collect();
        let num = parts[1];

        let game = Game::new(String::from(num), String::from(pulls));
        res += game.power;
    }

    println!("{}", res);
}
