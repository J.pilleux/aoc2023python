use phf::phf_map;

extern crate common;

static CORESP: phf::Map<&'static str, &'static str> = phf_map! {
    // Combos
    "oneight" => "18",
    "twone" => "21",
    "threeight" => "38",
    "fiveight" => "58",
    "eightwo" => "82",
    "eighthree" => "83",
    "sevenine" => "79",
    "nineight" => "98",
    // Simple
    "one" => "1",
    "two" => "2",
    "three" => "3",
    "four" => "4",
    "five" => "5",
    "six" => "6",
    "seven" => "7",
    "eight" => "8",
    "nine" => "9",
};

fn convert(line: String) -> String {
    let mut tmp = String::from(line);
    for (key, value) in CORESP.into_iter() {
        if !tmp.contains(key) {
            continue;
        }
        tmp = tmp.replace(key, value)
    }
    return tmp
}

fn main() {
    let content = include_str!("./input.txt");
    let values = content.split("\n");

    let mut total = 0;
    for line in values {
        if line.is_empty() {
            continue;
        }

        let converted = convert(String::from(line));
        let numbers: String = converted.chars().filter(|c| c.is_digit(10)).collect();
        let n1 = numbers.chars().next().unwrap();
        let n2 = numbers.chars().last().unwrap();
        let num = format!("{}{}", n1, n2);
        total += num.parse::<i32>().unwrap();
    }

    println!("{}", total);
}
