use std::{io::{BufReader, BufRead}, fs::File, path::Path};

pub fn lines_from_file(filename: impl AsRef<Path>) -> Vec<String> {
    let file = File::open(filename).expect("File not found");
    let buf = BufReader::new(file);
    buf.lines()
        .map(|line| line.expect("Could not parse this line"))
        .collect()
}

